import subprocess

def get_ip():
    content = subprocess.run(['ip','addr','show'], capture_output=True).stdout.decode()
    # print(content)
    res = [ elmt for elmt in content.splitlines() if ("inet " in elmt) and  (not "127.0.0.1" in elmt) ]
    res = [ elmt.split("/") for elmt in res[0].split(' ') if ("/") in elmt]
    ip = res[0][0]
    return ip