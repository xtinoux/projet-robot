from flask import Flask, Response, render_template, url_for, request
from flask_socketio import SocketIO, send, emit

from picamera.array import PiRGBArray
import picamera
import cv2

from app.robot import Robot
 

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.bot = Robot()



socketio = SocketIO(app)

@socketio.on('message')
def handle_message(message):
    send(message)

@socketio.on('connected')
def handle_message(message):
    print("#"*75)
    print(message)
    #send(json, json=True)

@socketio.on('move')
def handle_mouvement(data):
    print("#"*75)
    print(data)
    app.bot.move(data["vec"], data["vitesse"])

@socketio.on('stop')
def handle_stop(data):
    print("#"*75)
    print("stop")
    app.bot.stop()

#
#cap = cv2.VideoCapture(0)
#cap.set(cv2.CAP_PROP_FRAME_WIDTH,640)
#cap.set(cv2.CAP_PROP_FRAME_HEIGHT,480)
#cap.set(cv2.CAP_PROP_FPS, 40)
#video = cv2.VideoCapture(0)

#video.set(3,640)
#video.set(4,480)
init = False

def gen(camera):
    try:
        camera.framerate = 25
    except:
        print("Prob. de camera")
        camera.close()
        camera = picamera.PiCamera()
        camera.framerate = 25
    #camera.resolution = (640, 480)
    camera.framerate = 25
    #rawCapture = PiRGBArray(camera, size=(640, 480))
    rawCapture = PiRGBArray(camera)
    try:
        stream = camera.capture_continuous(rawCapture, format="bgr", use_video_port=True, splitter_port = 0)
    except:
        print("#"*75)
        print("Exception du stream")
        stream = camera.capture_continuous(rawCapture, format="bgr", use_video_port=True, splitter_port = 2)
    for st in stream:
        frame = st.array
        rawCapture.truncate(0)
        ret, jpeg = cv2.imencode('.jpg', frame)
        frame = jpeg.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/', methods = ['GET', 'POST'])
def index():
    if request.method == "POST":
        action = request.form["action"]
        vitesse = request.form["vitesse"]
        app.bot.mouvement(action,vitesse)
    return render_template("controls.html")

@app.route('/cam')
def cam():
    return render_template("index.html")


@app.route('/video_feed')
def video_feed():
    # global video
    camera = picamera.PiCamera()
    return Response(gen(camera),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    socketio.run(app,host='0.0.0.0', debug=True, port=5010)

