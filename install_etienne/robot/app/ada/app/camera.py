import cv2

class Video:
    def __init__(self):
        self.cap = cv2.VideoCapture(2)

    def set_resolution(self,width, height):
        self.cap.set(CV_CAP_PROP_FRAME_WIDTH,640)
        self.cap.set(CV_CAP_PROP_FRAME_HEIGHT,480)

    def get_frame(self):
        ret, self.frame = self.cap.read()
        print(f"ret : {ret}")
        ret, jpeg = cv2.imencode('.jpg', self.frame)        
        return jpeg.tobytes()

    def view_video(self):
        while True:
            _, self.frame = self.cap.read()
            cv2.imshow('Capture de la camera',self.frame)
            key = cv2.waitKey(10)
            if cv2.waitKey(10) & 0xFF == ord('q'):
                break

if __name__ == '__main__':
    vid = Video()
    vid.get_frame()