import explorerhat as hat

class Robot():
    def __init__(self):
        self.hat = hat

    def move(self, vec, vitesse):
        x = max(int(vitesse*vec[0]),100),
        y = max(int(vitesse*vec[1]),100)
        print(x,y)
        if x > 0:
            self.hat.motor.one.forwards(x)
        else:
            self.hat.motor.one.forwards(x)
        if y > 0:
            self.hat.motor.two.forwards(y)
        else:
            self.hat.motor.two.forwards(y)
    
    def stop(self):
        self.hat.motor.stop()

    def mouvement(self,action,vitesse):
        try:
            vitesse = int(vitesse)
        except:
            print("Erreur dans le transtypage")
            vitesse = 50
        if action == "forward":
            self.hat.motor.one.forwards(100)
            self.hat.motor.two.forwards(100)
        elif action == "backward":
            self.hat.motor.backwards(vitesse)
        elif action == "left":
            self.hat.motor.one.forwards(vitesse)
            self.hat.motor.two.backwards(vitesse)
        elif action == "right":
            self.hat.motor.one.forwards(vitesse)
            self.hat.motor.two.backwards(vitesse)
        elif action == "stop":
            self.hat.motor.stop() 

