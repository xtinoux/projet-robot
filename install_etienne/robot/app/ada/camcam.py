 
import picamera
from picamera.array import PiRGBArray
import cv2
import time

cam = cv2.VideoCapture(-1)


def gen():
    """Video streaming generator function."""
    while True:
        rval, frame = cam.read()
        cv2.imwrite('img.jpg', frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + open('img.jpg', 'rb').read() + b'\r\n')

if __name__ == "__main__":
    gen()