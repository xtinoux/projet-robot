var initX = 0;
var initY = 0;

function handleStart(evt) {
    var elmt = document.getElementById("myDiv");
    evt.preventDefault();
    var touches = evt.changedTouches;
    for (var i = 0; i < touches.length; i++) {
        initX = touches[i].pageX;
        initY = touches[i].pageY;
        console.log(touches[i].pageY);
    }
}

function handleMove(evt) {
    var elmt = document.getElementById("myDiv");
    evt.preventDefault();
    var touches = evt.changedTouches;
    for (var i = 0; i < touches.length; i++) {
        var posX = touches[i].pageX;
        var posY = touches[i].pageY;
        var vec = [posX - initX, posY - initY]
        var norme = Math.sqrt(vec[0] ** 2 + vec[1] ** 2);
        if (norme < 1) {
            norme = 1;
        }
        var vecNormalise = [vec[0] / norme, vec[1] / norme]
        elmt.innerHTML = vecNormalise[0];
        // console.log(touches[i].pageY);
    }
}


function handleStop(evt) {
    var elmt = document.getElementById("myDiv");
    evt.preventDefault();
    var touches = evt.changedTouches;
    for (var i = 0; i < touches.length; i++) {
        elmt.innerHTML = "0";
        // console.log(touches[i].pageY);
    }
}

function init() {
    var isTouchCapable = 'ontouchstart' in window ||
        window.DocumentTouch && document instanceof window.DocumentTouch ||
        navigator.maxTouchPoints > 0 ||
        window.navigator.msMaxTouchPoints > 0;

    if (isTouchCapable) {
        let elmt = document.getElementById("wrapper-controls")
        elmt.addEventListener("touchstart", handleStart, false);
        elmt.addEventListener("touchmove", handleMove, false);
        elmt.addEventListener("touchend", handleStop, false);
        elmt.addEventListener("touchleave", handleStop, false);
    }
}