import explorerhat as eh
from flask import Flask, render_template
import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import RPi.GPIO as GPIO

# Initialisation de la caméra

app = Flask(__name__)

@app.route("/")
@app.route("/<state>")
def update_robot(state=None):
    if state == 'avant':
        eh.motor.one.backwards(100)
        eh.motor.two.forwards(100)
    if state == 'arrière':
        eh.motor.one.forwards(100)
        eh.motor.two.backwards(100)
    if state == 'gauche':
        eh.motor.two.stop()
        eh.motor.one.backwards(100)
    if state == 'droite':
        eh.motor.one.stop()
        eh.motor.two.forwards(100)
    if state == 'stop':
        eh.motor.one.stop()
        eh.motor.two.stop()
    if state == 'sens-anti-horaire':
        eh.motor.one.backwards(100)
        eh.motor.two.backwards(100)
    if state == 'sens-horaire':
        eh.motor.one.forwards(100)
        eh.motor.two.forwards(100)
    if state == 'camera':
        camera=PiCamera()
        rawCapture=PiRGBArray(camera)
#Acquisition d'une image
        camera.start()
        camera.capture(rawCapture,format='bgr')
        image=rawCapture.array
        camera.close()
#Enregistrement de l'image
        cv2.imwrite('image.jpg',image)
    if eh.touch.one.is_pressed():
        eh.light.blue.on()
        time.sleep(0.2)
        eh.light.yellow.on()
        time.sleep(0.2)
        eh.light.red.on()
        time.sleep(0.2)
        eh.light.green.on()
        time.sleep(0.2)
        eh.light.off()
        eh.motor.one.stop()
        eh.motor.two.stop()
        camera.close()
    template_data = {
        'title' : state,
    }
    return render_template('main.html', **template_data)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5011)
