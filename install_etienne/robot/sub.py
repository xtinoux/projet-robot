import os
import subprocess
import sys
# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)

main = f"{dossier}\\test.py"
print(main)
process = subprocess.Popen(["python",main],
                            bufsize=1,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            close_fds=True,
                            universal_newlines=True)

for line in iter(process.stdout.readline, b''):
    line = line.strip()
    if line:
        print(line)