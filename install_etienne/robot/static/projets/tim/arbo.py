import glob 
import os.path 


def listdirectory(path, arbo, dossiers):
    l = glob.glob(os.path.join(path,"*"))
    for elmt in l:
        if os.path.isdir(elmt):
            dic_elmt = {"nom":elmt, "type":"dossier", "parent":path}
            arbo.append(dic_elmt)
            dossiers.append(dic_elmt)
        else:
            arbo.append({"nom":elmt, "type": "fichier", "parent":path})
        


def create_arbo(path): 
    arbo = []
    dossiers = []
    listdirectory('.', arbo, dossiers)
    while dossiers:
        dossier = dossiers.pop()
        listdirectory(os.path.join(dossier["nom"]), arbo, dossiers)

        
    return arbo

print(create_arbo("."))
