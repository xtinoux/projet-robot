class Noeud:    
    def __init__(self, content):
        self.content = content
        self.childs = []

    def is_leaf(self):
        if self.childs:
            return False
        return True
    
    def add_child(self, noeud):
        """
        Ajoute un enfant

        @param : Noeud
        @return : None
        """
        if not isinstance(noeud, Noeud):
            raise TypeError
        self.childs.append(noeud)

    def add_childs(self,noeuds):
        """
        Ajoute des enfants
        
        @param : Liste de Noeuds
        @return : None
        """
        for noeud in noeuds:
            if not isinstance(noeud, Noeud):
                raise TypeError
        self.childs.extend(noeuds)

    def split_content(self, prefixe):
        pass

class Radix():    
    def __init__(self, noeud):
        self.root = noeud

    def recherche(self, prefixe):
        """
        @param : chaine de caractères
        @return : liste de prefixe
        """
        pass

    def inserer(self):
        """
        Ajouter 

        """
        pass

    def proposition(self):
        """
        Parcours d'un graphe en profondeur
        """
        from collections import deque

        visited = []
        stack = deque()
        stack.append(self.root.content)

        while stack:
            node = stack.pop()
            if node not in visited:
                visited.append(node)
                if self.root.childs:
                    unvisited = [n.content for n in self.root.childs if n not in visited]
                    stack.extend(unvisited)

        return visited

 

    def recherche(self, prefixe):
        """
        Retourne une liste de prefixe qui ont le même préfixe 
        """
        if not prefixe:
            return self.parcours()
        elif not self.root.childs or not prefixe:
            return False
        else:
            for cellule in self.root.childs:
                if cellule.content in prefixe:
                    sous_arbre = Radix(cellule)
                    prefixe = prefixe.replace(cellule.content, "")
                    return sous_arbre.recherche(prefixe)
            return False

    def search(self, prefixe):
        """
        Recherche un mot
        """
        print(prefixe)
        print(self.root)
        if  not prefixe:
            return self.proposition()
        elif not self.root.childs:
            return False
        else:
            for node in self.root.childs:
                if node.content in prefixe:
                    print(prefixe, node.content)
                    subtree = Radix(node)
                    prefixe = prefixe.replace(node.content, "")
                    return subtree.search(prefixe)
            return False

    def affichage(self):
        print([ elmt.content for elmt in self.root.child])

f1 = Noeud("e")
f2 = Noeud("us")
f3 = Noeud("an") 
f3.add_childs([f1,f2])
f4 = Noeud("ulus")
f5 = Noeud("om")
f4.add_childs([f3, f4])
f7 = Noeud("r")
f7.add_child(f5)
r = Noeud(None)
r.add_child(f7)

arbre = Radix(r)
print(arbre.search("r"))
