import os
from flask import Flask, request, render_template
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
import uuid
import time 
from multiprocessing import Process, Lock
import requests
# Perso
from utils import get_ip
from arbre_radix import Arbre
from mots_cles import *

# On se place dans le dossier du script
main_dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(main_dossier)
# Construction des dossiers / fichiers nécessaires à l'application:
base = os.path.join("static", "projets")
for dossier_parent in ["ada", "alan", "grace","tim", "guido"]:
    try:
        os.makedirs(os.path.join(base, dossier_parent, "templates"))
    except:
        pass
    for dossier in ["css", "js", "medias"]:
        try:
            os.makedirs(os.path.join(base, dossier_parent, "static", dossier))
            print(f"Création du dossier {dossier} dans {base}/{dossier_parent} ")
        except:
            pass
    for key, value in {"css":"style.css", "js":"app.js"}.items():
        fichier = os.path.join(base, dossier_parent, "static", key, value)
        with open(fichier, "a") as f : 
            pass
# .......................................................

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif', "css", "js", "html", "webm", "webp", "mp4", "mp3", "ogg"])
app = Flask(__name__, static_folder="static", template_folder="templates")
socketio = SocketIO(app, async_mode=None)

# Utile au démmarage des serveurs
lock = Lock()
servers = []
PORTS = {"ada": "5010", "grace":"5011", "alan":"5012", "tim":"5013", "guido":"5014"}
####################################################################################
# Construction des différents arbres
arbre_python = Arbre(MOTS_CLES_PYTHON)
arbre_css = Arbre(MOTS_CLES_CSS)
arbre_js = Arbre(MOTS_CLES_JS)
arbre_html = Arbre(MOTS_CLES_HTML)
####################################################################################

####################################################################################
# 
# Socket IO
# 
@socketio.on('edition')
def edition(payload):
    # put in db
    # A mettre avec
    emit('edition',
        payload,
        broadcast=True)
    payload["content"] = ""
    mot = payload['search']
    separateurs = [" ",".",":","\n","\t",",","[","]","(",")","{","}"]
    for sep in separateurs:
        mot = mot.split(sep)[-1]
    print(mot)
    if payload["extension"] == "css":
        res = arbre_css.recherche_mot(mot)
    elif payload["extension"] == "js":
        res = arbre_js.recherche_mot(mot)
    elif payload["extension"] == "html":
        res = arbre_html.recherche_mot(mot)
    elif payload["extension"] == "py":
        print(arbre_python)
        res = arbre_python.recherche_mot(mot)
    payload["results"] = res
    print(res)
    print(payload["extension"])
    emit('autocompletion',
        payload,
        broadcast=True)

# Reception d'une requete save de la part de l'éditeur
@socketio.on('save')
def save(payload):
    # Sauvegarde du fichier
    base = os.path.join('static','projets',payload['room'], payload["dossier"])
    file_path = os.path.join(base,payload['file'])
    with open(file_path,"w") as f:
        f.write(payload["content"])
    socketio.emit("process", {"output":f"Sauvegarde du fichier {payload['file']}"}) 
 

# Reception d'une requete de démarage de serveur 
@socketio.on('start_server')
def start_server(payload):
    importation = 0
    try:
        lock.acquire()
        # Importation de l'application
        if payload['room'] == "grace":
            try:
                from static.projets.grace.main import app
                importation = 1
            except Exception as e:
                socketio.emit("process",{"output": f"Erreur lors de l'importation du projet : \n {e}"})        
        elif payload['room'] == "ada":
            try:
                from static.projets.ada.main import app
                importation = 1
            except Exception as e:
                socketio.emit("process",{"output": f"Erreur lors de l'importation du projet : \n {e}"})
        elif payload['room'] == "guido":
            try:
                from static.projets.guido.main import app
                importation = 1
            except Exception as e:
                socketio.emit("process",{"output": f"Erreur lors de l'importation du projet : \n {e}"}) 
        elif payload['room'] == "alan":
            try:
                from static.projets.alan.main import app
                importation = 1
            except Exception as e:
                socketio.emit("process",{"output": f"Erreur lors de l'importation du projet : \n {e}"})  
        elif payload['room'] == "tim":
            try:
                from static.projets.tim.main import app
                importation = 1
            except Exception as e:
                socketio.emit("process",{"output": f"Erreur lors de l'importation du projet : \n {e}"})  
      
        if importation:
            try:
                server = Process(target=app.run, kwargs={"debug":True, "host":"0.0.0.0", "port":PORTS[payload['room']]} )
                servers.append(server)
                server.start()
                socketio.emit("process",{"output": f"""Démarrage du serveur sur le port {PORTS[payload['room']]} \n <a href="http://projetrobot:{PORTS[payload['room']]} target=_blanck> http://sdfsdfs:{PORTS[payload['room']]}</a>"""})        
            except Exception as e:
                socketio.emit("process",{"output": f"Erreur lors du démarrage du serveur : \n {e}"})
                print(e)
    except Exception as e:
        socketio.emit("process",{"output": f"Erreur lors de l'importation du projet : \n {e}"})        
        print(e)

# Reception d'une requete de démarage de serveur 
@socketio.on('stop_server')
def stop_server(payload):
    print(servers)
    for server in servers:
        server.terminate()
        server.join()
        servers.remove(server)
    if not servers:
        lock.release()
        socketio.emit("process",{"output": f"Arret des serveurs en cours"})
        print("Arret du server en cours")


# Reception de l'ouverture d'un fichier par le client
@socketio.on('open_file')
def open_file(payload):
    print(payload)
    if payload["dossier"]:
        base = os.path.join('static','projets',payload['room'],payload["dossier"])
    else:
        base = os.path.join('static','projets',payload['room'])
    file_path = os.path.join(base,payload['file'])
    with open(file_path,'r') as f:
        payload["content"] = f.read()
    print(payload)
    emit('open_file',
        payload,
        broadcast=False)

# Reception de l'ouverture d'un fichier par le client
@socketio.on('create_file')
def create_file(payload):
    base = os.path.join('static','projets',payload['room'],payload['dossier'])
    file_name = payload['file']
    file_path = os.path.join(base, file_name)
    if os.path.exists(file_path):
        emit('process',{"output":"Le fichier existe déjà" }, broadcast=False)
        with open(file_path,'r') as f:
            payload["content"] = f.read()
            emit('open_file',
                payload,
                broadcast=False)  
    else:
        emit('process',{"output":"Création du fichier" }, broadcast=False)
        f = open(file_path,"w")
        f.close()
        with open(file_path,'r') as f:
            payload["content"] = f.read()
            emit('open_file',
                payload,
                broadcast=False)       
#
# Routage des pages de l'application
# 
@app.route("/")
def index():
    return render_template("index.html") 

@app.route("/editeur/", methods=["GET"])
@app.route("/editeur/<room>",  methods=["GET", "POST"])
def editeur(room = None):
    if request.method == 'POST':
        dossier = os.path.join('static','projets',room,request.form["dossier"])
        fichier = request.files['file']
        filename = fichier.filename
        if filename.split(".")[-1] in ALLOWED_EXTENSIONS:
            fichier.save(os.path.join(dossier, filename))
    if not room:
        return render_template("index.html") 
    adresse = get_ip()
    dossiers =  [ dossier for dossier in os.listdir(f"./static/projets/{room}/")  if not  os.path.isfile(os.path.join(f"./static/projets/{room}", dossier))]
    templates =  [ fichier for fichier in os.listdir(f"./static/projets/{room}/templates/")  if   os.path.isfile(os.path.join(f"./static/projets/{room}/templates", fichier))]
    static  =  [ fichier for fichier in os.listdir(f"./static/projets/{room}/static/")  if  os.path.isfile(os.path.join(f"./static/projets/{room}/static", fichier))]
    medias   =  [ fichier for fichier in os.listdir(f"./static/projets/{room}/static/medias/")  if  os.path.isfile(os.path.join(f"./static/projets/{room}/static/medias", fichier))]
    css     =  [ fichier for fichier in os.listdir(f"./static/projets/{room}/static/css/")  if  os.path.isfile(os.path.join(f"./static/projets/{room}/static/css", fichier))]
    js      =  [ fichier for fichier in os.listdir(f"./static/projets/{room}/static/js/")  if  os.path.isfile(os.path.join(f"./static/projets/{room}/static/js", fichier))]
    files = [ elmt for elmt in os.listdir(f"./static/projets/{room}/") if os.path.isfile(os.path.join(f"./static/projets/{room}", elmt)) ]
    return render_template("editeur.html", room = room, room_name= room.capitalize(), adresse=adresse, user=uuid.uuid4(), dossiers=dossiers, files=files, static=static, css=css, js=js, medias=medias, templates=templates)


@app.route("/send/<text>")
def send(text=None):
    socketio.emit("process",
        {"output":text })
    return "message envoyer"

# 
#  Démarrage de l'application
# 


if __name__ == '__main__':
    socketio.run(app,host="0.0.0.0", port=5678, debug=True)
