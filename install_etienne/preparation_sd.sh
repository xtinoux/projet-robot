
echo "Configuration de l'OS, taper 'C' pour continuer ou a pour annuler"
read -p "Votre choix (a/C) : " rep
if [ "$rep" = "a" ]; then
	echo "Operation annulee."
else
	echo -e "Veuillez patienter 5 secondes "
	sleep 1
	echo -e "Veuillez patienter 4 secondes "
	sleep 1
	echo -e "Veuillez patienter 3 secondes "
	sleep 1
	echo -e "Veuillez patienter 2 secondes "
	sleep 1
	echo -e "Veuillez patienter 1 secondes "
	sleep 1

	cmd=$(mount | grep -i rootfs | awk -F " " '{print $1}')
	part_boot=$(mount | grep -i boot | awk -F " " '{print $3}')
	part_root=$(mount | grep rootfs | awk -F " " '{print $3}')

	echo "Carte SD : "$(echo $cmd | sed -re 's/(sd[a-z])[1-9]*$/\1/')

	if [ -n "$cmd" ]; then
		sudo touch $part_boot/ssh
		mkdir $part_root/home/pi/.gestion_robot
		cp man_robot/man_robot $part_root/home/pi/.profile
		cp -ar man_robot/* $part_root/home/pi/.gestion_robot
		sudo mkdir $part_root/var
		sudo mkdir $part_root/var/www
		sudo mkdir $part_root/var/www/html
		sudo mkdir $part_root/var/www/html/robot
		sudo chmod a=rwxs $part_root/var/www/html/robot
		cp -ar robot/* $part_root/var/www/html/robot
		echo "La copie des fichiers est terminée"
	else
		echo "Probleme : carte SD non trouver"
		echo "Aller dans le dossier BOOT de la carte SD"
		echo "Tapez dans une console -touch ssh-"
		read -p "Tapez - ENTREEE - pour continuer"
	fi
	rep="n"
	while [ -z $rep ] || [ $rep != 'o' ]; do
		echo "Entrer le SSID de votre WIFI"
		read a
		echo "Confirmer (tapez o) ?"
		read rep
	done
	rep="n"
	while [ -z $rep ] || [ $rep != 'o' ]; do
		echo "Entrer le mot de passe de votre WIFI ($a)"
		read b
		echo "Confirmer (tapez o) ?"
		read rep
	done
	printf "%s\n" country=FR | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf
	printf "%s\n\t%s\n\t%s\n\t%s\n" network={ scan_ssid=1 ssid=\"$a\" psk=\"$b\" } | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf
	clear
	echo "----------------------------------------------------------------------------------------------------------"
	echo "Le Wifi sera operationnel au démarrage de votre Raspberry Pi et se connectera automatiquement (enfin normalement)."

	sed -r -i 's/^#alias/alias/' $part_root/home/pi/.bashrc

	echo "Pour la suite :"
	echo "1. Démmarrer votre Pi avec la carte SD"
	echo "2. Connecter vous sur le pi"
	echo "3. Le menu de configuration vous guidera dans la finalisation de votre installation"
	while [ -z $rep ] || [ $rep != 'o' ]; do
		echo "Merci de donner le mot de passe de votre WIFI ($a)"
		read b
		echo "Relisez bien ! Vous confirmez (tapez o) ?"
		read rep
	done
	printf "%s\n" country=FR | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf
	printf "%s\n\t%s\n\t%s\n\t%s\n" network={ scan_ssid=1 ssid=\"$a\" psk=\"$b\" } | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf

	echo "Le Wifi sera operationnel sur votre Raspberry Pi et se connectera automatiquement."

#config
	sudo sed -r -i 's/^([^#]\w*)/# \1/' $part_root/etc/locale.gen
	sudo sed -r -i 's/^# (fr_FR.UTF-8 UTF-8)/\1/' $part_root/etc/locale.gen
	
	sed -r -i 's/^#alias/alias/' $part_root/home/pi/.bashrc
	sudo cp -a $part_root/etc/rc.local $part_root/etc/rc.local.old
	sed -r -i 's/^#alias/alias/' $part_root/home/pi/.bashrc
	printf "%s\n" "export PATH=$PATH:/home/pi/.gestion_robot" | tee -a $part_root/home/pi/.bashrc
	
# fin config

	echo "Pour la suite :"
	echo "1. Démmarrer votre Pi avec la carte SD"
	echo "2. Connecter vous sur le pi"
	echo "3. Le menu de configuration vous guidera dans la finalisation de votre installation"
fi
