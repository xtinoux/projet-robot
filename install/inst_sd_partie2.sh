
echo "Configuration de l'OS, taper 'c' pour continuer"
rep=""
until [ "$rep" = "c" ] || [ "$rep" = "a" ]; do
	read -p "Votre choix (a/c) : " rep
done
if [ "$rep" = "c" ]; then
	read -t 5 -p "5 secondes d'attente..."
	echo "... et on repart."

	cmd=$(mount | grep -i rootfs | awk -F " " '{print $1}')
	part_boot=$(mount | grep -i boot | awk -F " " '{print $3}')
	part_root=$(mount | grep rootfs | awk -F " " '{print $3}')

	echo "Carte SD : "$(echo $cmd | sed -re 's/(sd[a-z])[1-9]*$/\1/')

	if [ -n "$cmd" ]; then
		sudo touch $part_boot/ssh
		mkdir $part_root/home/pi/man_robot
		cp -ar man_robot/* $part_root/home/pi/man_robot
		mkdir $part_root/home/pi/robot
		cp -ar robot/* $part_root/home/pi/robot
		echo "Terminer. Fichiers copies"
	else
		echo "Probleme : carte SD non trouver"
		echo "Aller dans le dossier BOOT de la carte SD"
		echo "Tapez dans une console -touch ssh-"
		read -p "Tapez - ENTREEE - pour continuer"
	fi

	rep="n"
	while [ -z $rep ] || [ $rep != 'o' ]; do
		echo "Merci de donner le SSID de votre WIFI"
		read a
		echo "Relisez bien ! Vous confirmez (tapez o) ?"
		read rep
	done
	rep="n"
	while [ -z $rep ] || [ $rep != 'o' ]; do
		echo "Merci de donner le mot de passe de votre WIFI ($a)"
		read b
		echo "Relisez bien ! Vous confirmez (tapez o) ?"
		read rep
	done
	printf "%s\n" country=FR | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf
	printf "%s\n\t%s\n\t%s\n\t%s\n" network={ scan_ssid=1 ssid=\"$a\" psk=\"$b\" } | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf

	echo "Le Wifi sera operationnel sur votre Raspberry Pi et se connectera automatiquement."

	sed -r -i 's/^#alias/alias/' $part_root/home/pi/.bashrc

	echo "Pour la suite :"
	echo "1. Démmarrer votre Pi avec la carte SD"
	echo "2. Aller dans le dossier -inst_robot-"
	echo "3. Tapez -./man_robot- et suivez le menu interractif"
	while [ -z $rep ] || [ $rep != 'o' ]; do
		echo "Merci de donner le mot de passe de votre WIFI ($a)"
		read b
		echo "Relisez bien ! Vous confirmez (tapez o) ?"
		read rep
	done
	printf "%s\n" country=FR | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf
	printf "%s\n\t%s\n\t%s\n\t%s\n" network={ scan_ssid=1 ssid=\"$a\" psk=\"$b\" } | sudo tee -a $part_root/etc/wpa_supplicant/wpa_supplicant.conf

	echo "Le Wifi sera operationnel sur votre Raspberry Pi et se connectera automatiquement."

#config
	sudo sed -r -i 's/^([^#]\w*)/# \1/' $part_root/etc/locale.gen
	sudo sed -r -i 's/^# (fr_FR.UTF-8 UTF-8)/\1/' $part_root/etc/locale.gen
	
	sed -r -i 's/^#alias/alias/' $part_root/home/pi/.bashrc
	sudo cp -a $part_root/etc/rc.local $part_root/etc/rc.local.old
	sed -r -i 's/^#alias/alias/' $part_root/home/pi/.bashrc
	printf "%s\n" "export PATH=$PATH:/home/pi/man_robot" | tee -a $part_root/home/pi/.bashrc
	
# fin config

	echo "Pour la suite :"
	echo "1. Démmarrer votre Pi avec la carte SD"
	echo "2. Aller dans le dossier -man-robot-"
	echo "3. Tapez -./man_robot- et suivez le menu interractif"
else
	echo "Operation annulee."
fi
