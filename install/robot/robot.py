#!/usr/bin/env python

from flask import Flask, render_template, Response, request, redirect, url_for
import explorerhat as eh
#from flask_cors import cross _origin
from flask_socketio  import SocketIO, emit


## import camera driver
#if os.environ.get('CAMERA'):
#    Camera = import_module('camera_' + os.environ['CAMERA']).Camera
#else:
#    from camera import Camera

# Raspberry Pi camera module (requires picamera package)
from camera_pi import Camera

async_mode = None

app = Flask(__name__, template_folder='templates', static_folder='static'))
socket_ = SocketIO(app, async_mode=async_mode)



@app.route('/', methods = ["GET", "POST"])
def index():
    """Video streaming home page."""
    return render_template('robot.html', sync_mode=socket_.async_mode)



def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
           b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@socket_.on('joy_event')
def test_joystick(message):
#    print({'X': message['X']})
#    emit('my_cam', {'X': message['X']})
    x = int(message["X"])
    y = -int(message["Y"])
    x = min(x, 100)
    x = max(x, -100)
    y = min(y, 100)
    y = max(y, -100)
    V = (x**2 + y**2)**0.5
    V = int(V / 1.42)
    if y >= 0:
        if x > 0:
            eh.motor.one.forward(V)
            eh.motor.two.forward(int(V*(1-x/100)))
        if x < 0:
            eh.motor.one.forward(int(V*(1+x/100)))
            eh.motor.two.forward(V)      
    if y <= 0:
        if x > 0:
            eh.motor.one.backward(V)
            eh.motor.two.backward(int(V*(1-x/100)))
        if x < 0:
            eh.motor.one.backward(int(V*(1+x/100)))
            eh.motor.two.backward(V)




if __name__ == '__main__':
#    app.run(host='0.0.0.0', port=8888, threaded=True)
    socket_.run(app, host='127.0.0.1', port=8888)
