from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Bonjour, Monde! <br> Comment ça va ?'

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5013, debug=True)