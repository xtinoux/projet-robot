from flask import Flask, render_template, url_for, request

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'


@app.route('/')
def index():
    return "Bonjour de la part de Tim, Bravo"


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5012)
