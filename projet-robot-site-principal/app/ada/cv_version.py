from flask import Flask, Response, render_template, url_for
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2


app = Flask(__name__)
video = cv2.VideoCapture(2)

def gen(video):
    while True:
        success, image = video.read()
        ret, jpeg = cv2.imencode('.jpg', image)
        frame = jpeg.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/video_feed')
def video_feed():
    # global video
    return Response(gen(video),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/game')
def game():
	return render_template("game.html")

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5010, threaded=True)
