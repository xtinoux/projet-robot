import os 
import time
import subprocess 
from eventlet.green  import subprocess as sp
# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)

def thread_start_server(room):
    print(f"Démarrage du serveur {room}")
    cmd =["python3", os.path.join(dossier,"app",room,'main.py')]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE,stderr=subprocess.PIPE, bufsize=1)
    print(process)
    while True:
        print("Bonjour")
        output = process.stdout.readline()
        error = process.stderr.readline()
        if process.poll() is not None:
            break
        if output:
            output = output.decode('utf-8').strip()
            print(output)
            emit("process", {
                    "room" : room,
                    "output": output})
        if error:
            error = error.decode('utf-8').strip()
            print(error)

def simple_start_server(room):
    print(f"Démarrage du serveur {room}")
    cmd =["python3", os.path.join(dossier,"app","alan",'main.py')]
    print(cmd)
    # process = subprocess.Popen(cmd, stdout=subprocess.PIPE,stderr=subprocess.PIPE, bufsize=1)
    process = subprocess.run(cmd, capture_output=True)
    print(process)


def eventlet_start_server(room):
    print(f"Démarrage du serveur {room}")
    cmd =["python3", os.path.join(dossier,"app","alan",'main.py')]
    print(cmd)
    process = sp.Popen(cmd, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # process = subprocess.run(cmd, capture_output=True)
    print(process.pid)
    print(process.poll())
    while True :
        output = process.stdout.readline()
        error = process.stderr.readline()
        # if is not None:
        #     break
        if output:
            print("Outpuit")
            output = output.decode('utf-8').strip()
            print(output)
            # emit("process", {
            #         "room" : room,
            #         "output": output})
        if error:
            error = error.decode('utf-8').strip()
            print(error)
            # emit("process", {
            #         "room" : room,
            #         "error": error})
        time.sleep(0.5)

if __name__ == "__main__":
    from threading import Thread 

    th1 = Thread(target=eventlet_start_server, args=("alan",))
    th1.start()