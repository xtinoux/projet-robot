import os
import subprocess
from main import socketio,app
 
# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)

cmd =["python3", os.path.join(dossier,'main.py')]
process = subprocess.Popen(cmd, stdout=subprocess.PIPE,stderr=subprocess.PIPE, bufsize=1)
while True:
  output = process.stdout.readline()
  if process.poll() is not None:
    break
  if output:
    output = output.decode('utf-8').strip()
    print(output.strip())