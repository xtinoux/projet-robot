import os
from os.path import isfile, join
from flask import Flask, request, render_template
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
import uuid

app = Flask(__name__,static_folder="static", template_folder="templates")
socketio = SocketIO(app, async_mode=None)

# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)

import sys
from asyncio.subprocess import PIPE
from contextlib import closing

async def readline_and_kill(*args):
    # start child process
    process = await asyncio.create_subprocess_exec(*args, stdout=PIPE)

    # read line (sequence of bytes ending with b'\n') asynchronously
    async for line in process.stdout:
        print("got line:", line.decode(locale.getpreferredencoding(False)))
        break
    process.kill()
    return await process.wait() # wait for the child process to exit
    loop = asyncio.get_event_loop()

with closing(loop):
    sys.exit(loop.run_until_complete(readline_and_kill(
        "python3", os.path.join(dossier,"app","main.py"))))
# 
# Socket IO
# 
@socketio.on('edition')
def edition(payload):
    # put in db
    # A mettre avec
    print(payload)
    emit('edition',
        payload,
        broadcast=True)

# Reception d'une requete save de la part de l'éditeur
@socketio.on('save')
def save(payload):
    # put in db
    # A mettre avec
    base = join('app',payload['room'])
    file_path = join(base,payload['file'])
    with open(file_path,"w") as f:
        f.write(payload["content"])  
 
# Reception d'une requete de démarage de serveur 
@socketio.run('start_server')
def start_server(playload):
    cmd = f"python3 {}main.py"
    subprocess.run(args, *, stdin=None,  stdout=None, stderr=None, capture_output=True)

# Reception de l'ouverture d'un fichier par le client
@socketio.on('open_file')
def open_file(payload):
    base = join('app',payload['room'])
    file_path = join(base,payload['file'])
    with open(file_path,'r') as f:
        payload["content"] = f.read()
    print(payload)
    emit('open_file',
        payload,
        broadcast=False)


# 
#  Application
# 
@app.route("/")
def index():
    return render_template("index.html") 

@app.route("/editeur/")
@app.route("/editeur/<room>")
def editeur(room = None):
    dossiers =  [ dossier for dossier in os.listdir(f"./app/{room}/")  if not  isfile(join("./app/alan", dossier))]
    files = [ elmt for elmt in os.listdir(f"./app/{room}/") if isfile(join("./app/alan", elmt)) ]
    print(files)
    return render_template("editeur.html", room = room, user=uuid.uuid4(), dossiers=dossiers, files = files)

# 
#  Démarrage de l'application
# 
if __name__ == '__main__':
    socketio.run(app,host="0.0.0.0", port=5678, debug=True)
