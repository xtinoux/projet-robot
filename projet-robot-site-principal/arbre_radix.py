from mots_cles import *

class Cellule():
    def __init__(self, content):
        self.content = content
        self.enfants = []

    def __str__(self):
        """
        Renvoi une chaine de caractère avec le contenu de la cellule et de ces descendants
        ex : [ contenu ,[ e1 ,[ se1-e1, se2-e1], [e2], [e3 ,[se1-e3]] ]]
        
        @params:
            - None
        
        @returns:
            - str
        """
        if not self.enfants:
            return self.content
        return  self. content + str([ enfant.__str__() for enfant in self.enfants ]).replace("\\","").replace("'","")
    
    def __contains__(self, text):
        """
        Retourne True si la 1 lettre du texte est aussi la première lettre du contenu de la cellule
        Sensible à la case

        @params:
            - text (str)
        
        @return:
            - booleen
        """
        try:
            if text[0] == self.content[0]:
                return True
            else:
                return False
        except:
            return False
    
    def  base_commune(self, text):
        """
        Renvoi les lettres communes entre le contenu de la cellule et le text

        @params:
            - text (str)

        @returns:
            - base (str)
        """
        base = ""
        for index in range(min(len(text), len(self.content))):
            if text[index] == self.content[index]:
                base += text[index]
            else:        
                return base
        return base
        
    def add_mot(self, mot):
        """
        Parcours les enfants de la cellule et modifie la listes enfants pour y inclure le mots à l'emplacement requis ! 
            - a la racine si la première lettre n'est pas dans l'arbre
            - une partie du mot en enfant d'une cellule si la cellule est entierement dans le mot
            - en séparant la cellule en 2 partie pour créer la partie commune et rajouter les 2 enfants

        @params:
            - mot (str)
        
        @return:
            - None 
        """
        etape = 1
        if not self.enfants:
            self.enfants.append(Cellule(mot))
        else:
            if not sum( mot in enfant for enfant in self.enfants):
                self.enfants.append(Cellule(mot))
            else:
                for enfant in self.enfants:
                    if mot in enfant:
                        etape += 1
                        base = enfant.base_commune(mot)
                        diff_base =  enfant.content.replace(base,"",1)
                        diff_mot =  mot.replace(base,"",1)
                        if diff_base and diff_mot:
                            """
                            Cas ou le contenue de la cellule de l'arbre est different de la base
                            On creer 2 cellule et on change le contenue de la première"
                            """
                            enfant.content = base
                            nv_enfant_1 = Cellule(diff_base)
                            for pt_enfant in enfant.enfants:
                                nv_enfant_1.enfants.append(pt_enfant)
                            enfant.enfants = [nv_enfant_1]
                            nv_enfant_2 = Cellule(mot.replace(base,"",1))
                            enfant.enfants.append(nv_enfant_2)
                        elif diff_base:
                            """
                            L'integralite du mot est dans la base , mais la base est plus longue'
                            """
                            enfant.content = base
                            nv_enfant_1 = Cellule(diff_base)
                            for pt_enfant in enfant.enfants:
                                nv_enfant_1.enfants.append(pt_enfant)
                            enfant.enfants = [nv_enfant_1]
                        elif diff_mot:
                            """
                            Le mot est plus long que la base
                            """
                            # print(f"etape {etape}")
                            enfant.add_mot(diff_mot)

    def parcours_enfants(self):
        """
        Fonction de debug.
        liste les enfants
        
        """
        if not self.enfants:
            return [self.content]
        else:
            return [self.content] + [ enfant.parcours_enfants() for enfant in self.enfants]

    def liste_mots(self,res = [], base =""):
        """
        Construction d'une liste de mot possible à partir du contenu d'une cellule
        @params
            - res (list, defaut -> []): les precedents resultats 
            - base (str, defaut -> ""): le contenu des cellules précédemment parcouru lors des appels récurssifs
        
        @returns
            - res (list) : Combinaison des contenus des cellules assemblable
        """ 
        lst = self.parcours_enfants()
        if not base:
            base = self.content
            res.append(self.content)
        base_origine = base
        for enfant in self.enfants:
            base = base + enfant.content
            res.append(base)
            enfant.liste_mots(res,base)
            base = base_origine        
        return res

    def recherche_mot(self, mot, prefixe):
        """
        Recherhce du mot
        @params
            - mot: text
            - prefixe: le contenu des cellules précédemment parcouru lors des appels récurssifs
        
        @returns
            - tuple res (liste de possibilités), prefixe (liste des préfixes parcourus)
        """ 
        res = []
        for enfant in self.enfants:
            if mot in enfant:
                base = enfant.base_commune(mot)
                diff_mot =  mot.replace(base,"",1)
                if not diff_mot:
                    return enfant.liste_mots(res=[], base="")
                else:
                    prefixe.append(base)
                    res = enfant.recherche_mot(diff_mot, prefixe)
            else:
                pass        
        return res , prefixe

class Arbre():
    def __init__(self, MOTS_CLES=[""], cellule = None, ):
        "Initialise l'arbre a partir de la base mots cles"
        if cellule:
            self.racine = cellule
        self.racine = Cellule("")
        for mot in MOTS_CLES:
            self.add_mot(mot)

    def add_cell(self, cellule):
        "Ajoute une cellule à la racine de l'arbre"
        self.racine.enfants.append(cellule)
        
    def __str__(self):
        return self.racine.__str__()

    def __contains__(self, mot):
        for enfant in self.racine.enfants:
            if mot in enfant:
                return True
        return False

    def add_mot(self, mot):
        self.racine.add_mot(mot)     

    def recherche_mot(self,mot):
        """
        Recherche les possibilités à partir d'un mot
        Reconstitue les mots en fonction des préfixe et suffixe obtenue par 
        """
        res = self.racine.recherche_mot(mot, [])
        suffixes = []
        prefixes = []
        if res:
            if isinstance(res, list):
                suffixes = res
            while isinstance(res, tuple):
                try:
                    suffixes = res[0]
                except:
                    pass
                try:
                    prefixes = res[1]
                except:
                    pass
                res = res[0]
        try:
            prefixe = "".join(prefixes)
        except: 
            prefixe = ""
        if suffixes:
            return  [prefixe + suffixe for suffixe in suffixes ]
        else:
            return [prefixe]

if __name__ == "__main__":
    radix = Arbre(MOTS_CLES_PYTHON)
    print(radix)


