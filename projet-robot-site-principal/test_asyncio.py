import os
from os.path import isfile, join
from flask import Flask, request, render_template
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
import uuid

app = Flask(__name__,static_folder="static", template_folder="templates")
socketio = SocketIO(app, async_mode=None)

# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)

import sys
import asyncio
from asyncio.subprocess import PIPE
from contextlib import closing
import locale
async def readline_and_kill(*args):
    # start child process
    process = await asyncio.create_subprocess_exec(*args, stdout=PIPE)
    print("redline")
    # read line (sequence of bytes ending with b'\n') asynchronously
    async for line in process.stdout:
        print("got line:", line.decode(locale.getpreferredencoding(False)))
    return await process.wait() # wait for the child process to exit



# "python3", os.path.join(dossier,"app","main.py"

@app.route("/")
def index():
    if sys.platform == "win32":
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)
        else:
            loop = asyncio.get_event_loop()
        with closing(loop):
            sys.exit(loop.run_until_complete(readline_and_kill(
                "ping","10.10.10.10")))
    return render_template("index.html")